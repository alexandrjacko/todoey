//
//  Category.swift
//  Todoey
//
//  Created by Alexandr Jacko on 29/11/2018.
//  Copyright © 2018 Alexandr Jacko. All rights reserved.
//

import Foundation
import RealmSwift

class Category : Object {
    @objc dynamic var name : String = ""
    let items = List<Item>()
    
    @objc dynamic var color : String?
}
